import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profile } from '../model/profile.interface';
import { ProfileRepository, createProfile } from './profile-repository.interface';
import { Measuring } from '../model/measuring.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class LocalStorageProfileRepositoryService implements ProfileRepository {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    private static readonly PROFILE_KEY = 'profile';
    // eslint-disable-next-line @typescript-eslint/naming-convention
    private static readonly VERSION_KEY = '0.0.1';

    constructor() { }

    getProfile(): Observable<Profile> {
        const dataToken = localStorage.getItem(this.getLocalStorageKey());

        if (!!dataToken) {
            try {
                const profile: Profile = JSON.parse(dataToken);

                if (typeof profile.birthday === 'string') {
                    profile.birthday = new Date(profile.birthday);
                }

                return of(profile);
            } catch (error) {
                console.error('Could not parse local storage entry', error);
            }
        }

        return of(createProfile());
    }

    getDefaultMeasurment(date: Date): Observable<Measuring> {
        return this.getProfile().pipe(
            map(profile => {
                const defaultMeasuring: Measuring = {
                    weight: profile.startingWeight,
                    unit: profile.weightUnit,
                    date,
                };

                return defaultMeasuring;
            }),
        );
    }

    updateProfile(profile: Profile): Observable<Profile> {
        localStorage.setItem(this.getLocalStorageKey(), JSON.stringify(profile));
        return of(profile);
    }

    private getLocalStorageKey(): string {
        return `${LocalStorageProfileRepositoryService.PROFILE_KEY}_${LocalStorageProfileRepositoryService.VERSION_KEY}`;
    }
}
