export type WeightUnit = 'kg' | 'pound';

export interface Measuring {
    idx?: number;
    date: Date;
    weight: number;
    unit: WeightUnit;
    projected?: boolean;
    consumed?: number;
}
