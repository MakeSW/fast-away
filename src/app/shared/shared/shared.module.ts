import { CommonModule, DatePipe, formatNumber } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { LocalStorageMeasuringRepositoryService } from '../services/local-storage-measuring-repository.service';
import { LocalStorageProfileRepositoryService } from '../services/local-storage-profile-repository.service';
import { MEASURING_REPOSITORY_TOKEN } from '../services/measuring-repository.interface';
import { PROFILE_REPOSITORY_TOKEN } from '../services/profile-repository.interface';
import { WeightProjectionService } from '../services/weight-projection.service';
import { ScrollHelperService } from '../services/scroll-helper.service';
import { FormatTrackedNumberPipe } from '../pipes/format-tracked-number.pipe';
import { StatusService } from '../services/status.service';
import { UnitConversionService } from '../services/unit-conversion.service';

@NgModule({
  declarations: [
      FormatTrackedNumberPipe,
  ],
  providers: [
    {
        provide: PROFILE_REPOSITORY_TOKEN,
        useClass: LocalStorageProfileRepositoryService,
    },
    {
        provide: MEASURING_REPOSITORY_TOKEN,
        useClass: LocalStorageMeasuringRepositoryService,
    },
    WeightProjectionService,
    DatePipe,
    ScrollHelperService,
    StatusService,
    UnitConversionService,
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild({ extend: true }),
  ],
  exports: [
        FormatTrackedNumberPipe,
  ],
})
export class SharedModule { }
