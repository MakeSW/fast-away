import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
    public appPages = [];

    public initialized = false;

    constructor(
        private readonly translate: TranslateService,
    ) {}

    ngOnInit(): void {
        const supportedLanguages = [ 'en', 'de' ];
        let appLanguage = this.translate.getBrowserLang();
        appLanguage = supportedLanguages.includes(appLanguage) ? appLanguage : supportedLanguages[0];

        this.translate.setDefaultLang(supportedLanguages[0]);
        this.translate.use(appLanguage).subscribe(_ => {
            this.initialized = true;
            this.initMenu();
        });
    }

    private initMenu(): void {
        this.appPages = [
            { title: this.translate.instant('overview_title'), url: '/overview', icon: 'person' },
            { title: this.translate.instant('profile_short_title'), url: '/profile', icon: 'cog' },
        ];
    }
}
