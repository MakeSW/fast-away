import { Injectable } from '@angular/core';

@Injectable()
export class UnitConversionService {
  constructor() { }

  convertPoundToKg(pounds: number): number {
    const unroundedPounds = pounds * 0.453592;
    return Math.floor(unroundedPounds * 100) / 100;
  }

  convertInchesToCm(inches: number): number {
      const unroundedInches = inches * 2.54;
      return Math.floor(unroundedInches * 100) / 100;
  }
}
