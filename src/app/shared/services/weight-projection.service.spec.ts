import { TestBed } from '@angular/core/testing';

import { WeightProjectionService } from './weight-projection.service';

describe('WeightProjectionService', () => {
  let service: WeightProjectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WeightProjectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
