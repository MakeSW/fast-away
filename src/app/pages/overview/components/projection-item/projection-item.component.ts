import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WeightProjection } from '../../../../shared/model/weight-projection.interface';

@Component({
  selector: 'faw-projection-item',
  templateUrl: './projection-item.component.html',
  styleUrls: ['./projection-item.component.scss'],
})
export class ProjectionItemComponent implements OnInit {
    @Input()
    projection: WeightProjection;

    @Input()
    previous: WeightProjection;

    @Output()
    tapped: EventEmitter<void> = new EventEmitter<void>();

    expanded = false;

    constructor() { }

    ngOnInit() {}

    toggleDetails(e: Event): void {
        e.preventDefault();
        e.stopPropagation();

        this.expanded = !this.expanded;
    }
}
