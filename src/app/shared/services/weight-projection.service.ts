import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

import { Measuring } from '../model/measuring.interface';
import { Profile } from '../model/profile.interface';
import { WeightProjection } from '../model/weight-projection.interface';
import { UnitConversionService } from './unit-conversion.service';

@Injectable()
export class WeightProjectionService {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    public static readonly CALORIES_PER_KG = 7700;

    private profile: Profile;
    private measurings: Measuring[];

    private readonly calculators: { [sex: string]: (weight: number, height: number, age: number) => number } = {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Male: (weight: number, height: number, age: number) => (88.362 + (13.397 * weight) + (4.799 * height) - (5.677 * age)),
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Female: (weight: number, height: number, age: number) => (447.593 + (9.247 * weight) + (3.098 * height) - (4.330 * age)),
    };

    constructor(
        private readonly datePipe: DatePipe,
        private readonly unitSrv: UnitConversionService,
    ) { }

    configure(profile: Profile, measurings: Measuring[]): void {
        this.profile = profile;
        this.measurings = measurings;
    }

    getProjections(start: Measuring, measurings: Measuring[], numOfProjections): WeightProjection[] {
        const dateMeasurementMap: { [key: string]: Measuring } = {};
        measurings.forEach(measuring => {
            const dateToken = this.datePipe.transform(measuring.date, 'yyyy-MM-dd');
            dateMeasurementMap[dateToken] = measuring;
        });
        const weightProjections: WeightProjection[] = [];
        const today = new Date();
        today.setHours(0, 0, 0, 0);

        let prev: Measuring = start;
        let caloricDeficit = 0;
        let dietOffset = 0;
        let daysOfStickingToPlan = 0;
        let hasPreviousEntry = false;

        for (let i = 0; i < numOfProjections; i++) {
            const offset = i + 1;
            const date = new Date(start.date);
            date.setDate(date.getDate() + offset);
            const isBeforeProjectionStart = !hasPreviousEntry && date < today;
            const key = this.datePipe.transform(date, 'yyyy-MM-dd');
            let current = dateMeasurementMap[key];

            if (!this.isKeepingToDietPlan(date, current, dietOffset)) {
                dietOffset++;
                daysOfStickingToPlan = 0;
            } else {
                daysOfStickingToPlan++;
            }

            const caloryRequirement = this.calculateCaloryRequirement(prev.weight);
            const plannedIntake = caloryRequirement * this.getCaloricDeficitFactor(date, dietOffset);

            if (!!current) {
                hasPreviousEntry = true;
                weightProjections.push({
                    ...current,
                    projectedWeight: current.weight,
                    plannedIntake,
                    daysOfStickingToPlan,
                });

                // if no measured calories are set, we assume the user consumed his complete
                // accumulated caloric deficit
                if ('consumed' in current && !isNaN(current.consumed)) {
                    const projection = this.calculateWeightDelta(
                        caloricDeficit,
                        current.consumed,
                        current.weight,
                    );
                    caloricDeficit = projection[0];
                } else {
                    caloricDeficit = 0;
                }
            } else {
                const projections = this.calculateWeightDelta(
                    caloricDeficit,
                    isBeforeProjectionStart ? caloryRequirement : plannedIntake,
                    prev.weight,
                );

                // eslint-disable-next-line max-len
                console.log(`Prev consumed: ${caloricDeficit}, planned intake ${plannedIntake}, resulting deficit ${projections[0]}`);

                caloricDeficit = projections[0];
                const weight = projections[1];
                current = {
                    date,
                    weight,
                    unit: start.unit,
                };

                weightProjections.push({
                    ...current,
                    caloricDeficit,
                    plannedIntake,
                    projectedWeight: weight,
                    projected: true,
                    daysOfStickingToPlan,
                });
            }

            prev = current;
        }

        return weightProjections;
    }

    getCaloricDeficitFactor(date: Date, offset: number): number {
        // TODO: figure out how to configure this better
        const day = this.getDayOfYear(date);
        const factor = (day + offset) % 2 === 0 ? 0 : 1;

        return factor;
    }


  calculateCaloryRequirement(currentWeight: number): number {
    const height = (this.profile.heightUnit === 'inch') ?
        this.unitSrv.convertInchesToCm(this.profile.height) : this.profile.height;
    const weight = (this.profile.weightUnit === 'pound') ?
        this.unitSrv.convertPoundToKg(currentWeight) : currentWeight;
    const sex = this.profile.sex;
    const calculateCalories = this.calculators[sex];
    const baseCaloryRequirement = calculateCalories(weight, height, this.getAge());
    const unroundedCaloryRequirement = baseCaloryRequirement * this.getActivityFactor();

    return Math.floor(unroundedCaloryRequirement * 100) / 100;
  }

  calculateWeightDelta(caloriesDeficit: number, caloriesConsumed: number, weight: number): [ number, number ] {
    const caloryRequirement = this.calculateCaloryRequirement(weight);
    const deficit = caloryRequirement - caloriesConsumed;
    let updatedDeficit = caloriesDeficit + deficit;
    let kgLost = 0;

    if (updatedDeficit >= WeightProjectionService.CALORIES_PER_KG) {
        kgLost = Math.floor(updatedDeficit / WeightProjectionService.CALORIES_PER_KG);
        updatedDeficit = updatedDeficit - (kgLost * WeightProjectionService.CALORIES_PER_KG);
    }

    return [ updatedDeficit, (weight - kgLost) ];
  }

    private isKeepingToDietPlan(date: Date, current: Measuring, offset: number): boolean {
        // in case we do not have a calories intake, we assume user kept to his plan
        if (!current || !('consumed' in current)) {
            return true;
        }

        const isExpectedToFast = this.getCaloricDeficitFactor(date, offset) === 0;
        const isFasting = current.consumed === 0 ||
            isNaN(current.consumed);

        return isExpectedToFast === isFasting;
    }

    private getDayOfYear(date: Date): number {
        const dayCount = [
            0,
            31, //jan
            28, //feb(non leap)
            31, //march
            30, //april
            31, //may
            30, //june
            31, //july
            31, //aug
            30, //sep
            31, //oct
            30, //nov
            31  //dec
        ];
        const isLeapYear = (new Date(date.getFullYear(), 1, 29).getMonth() === 1);
        dayCount[2] = isLeapYear ? 29 : 28;
        const month = date.getMonth();
        let days = date.getDate();

        for (let i = 0; i < month; i++) {
            days += dayCount[i];
        }

        return days;
    }

  private getActivityFactor(): number {
    switch (this.profile.activityLevel) {
        case 'Sedentary':
            return 1.2;
        case 'Lightly Active':
            return 1.375;
        case 'Moderately Active':
            return 1.55;
        case 'Very Active':
            return 1.725;
        case 'Extremely Active':
            return 1.9;
        default:
            return 1.2;
    }
  }

  private getAge(): number {
      const today = new Date();
      const birthday = this.profile.birthday;

      return today.getFullYear() - birthday.getFullYear();
  }
}

