import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { OverviewPageComponent } from './overview-page/overview-page.component';
import { OverviewPageRoutingModule } from './overview-routing.module';
import { MeasurementFormComponent } from './components/measurement-form/measurement-form.component';
import { ProjectionItemComponent } from './components/projection-item/projection-item.component';
import { SharedModule } from '../../shared/shared/shared.module';

@NgModule({
  declarations: [
      OverviewPageComponent,
      MeasurementFormComponent,
      ProjectionItemComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    OverviewPageRoutingModule,
    TranslateModule.forChild({ extend: true }),
  ]
})
export class OverviewModule { }
