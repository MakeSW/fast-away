import { DatePipe } from '@angular/common';
import { Component, Inject, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { forkJoin, Subscription } from 'rxjs';

import { MeasurmeneFormValue as MeasurmentFormValue } from '../../../../shared/model/measurement-form-value.interface';
import { Measuring } from '../../../../shared/model/measuring.interface';
import { Profile } from '../../../../shared/model/profile.interface';
import { MEASURING_REPOSITORY_TOKEN, MeasuringRepository } from '../../../../shared/services/measuring-repository.interface';
import { PROFILE_REPOSITORY_TOKEN, ProfileRepository } from '../../../../shared/services/profile-repository.interface';

@Component({
  selector: 'faw-measurement-form',
  templateUrl: './measurement-form.component.html',
  styleUrls: [ './measurement-form.component.scss' ],
})
export class MeasurementFormComponent implements OnInit, OnDestroy {
    @Input()
    date: Date;

    @Input()
    weight: number;

    @Input()
    consumed: number;

    @Input()
    idx: number | string;

    form: FormGroup = null;

    profile: Profile;

    private subscription = new Subscription();

    constructor(
        private readonly modalCtrl: ModalController,
        private readonly datePipe: DatePipe,
        @Inject(PROFILE_REPOSITORY_TOKEN) private readonly profileSrv: ProfileRepository,
        @Inject(MEASURING_REPOSITORY_TOKEN)  private readonly measuringSrv: MeasuringRepository,
    ) {}

    dismiss(): void {
        this.modalCtrl.dismiss();
    }

    accept(): void {
        this.form.updateValueAndValidity();

        if (this.form.valid) {
            this.modalCtrl.dismiss(this.getMeasurementValue());
        }
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this.subscription = null;
    }

    ngOnInit() {
        const $measurement = this.measuringSrv.getLatestMeasurement();
        const $profile = this.profileSrv.getProfile();

        forkJoin([ $measurement, $profile ]).subscribe(([ measurement, profile ]: [ Measuring, Profile ]) => {
            const weight = this.getWeightVal(profile, measurement);
            const consumed = this.getConsumedVal(measurement);
            const date = this.getDateVal(measurement);
            const dateVal = this.datePipe.transform(date, 'yyyy-MM-dd');

            this.profile = profile;

            this.form = new FormGroup({
                date: new FormControl(dateVal, [ Validators.required ]),
                weight: new FormControl(weight, [ Validators.required, Validators.min(0), Validators.max(1000) ]),
                consumed: new FormControl(consumed,  [ Validators.min(-20000), Validators.max(100000) ]),
            });
        });
    }

    private getDateVal(measurement: Measuring): Date {
        if (!!this.date) {
            return this.date;
        }

        if (!!measurement && !!measurement.date) {
            return measurement.date;
        }

        return new Date();
    }

    private getWeightVal(profile: Profile, measurement: Measuring): number {
        if (typeof this.weight === 'number') {
            return this.weight;
        }

        if (!!measurement && typeof measurement.weight === 'number') {
            return measurement.weight;
        }

        return profile.startingWeight;
    }

    private getConsumedVal(measurement: Measuring): number {
        if (typeof this.consumed === 'number') {
            return this.consumed;
        }

        return measurement?.consumed || null;
    }

    private getMeasurementValue(): MeasurmentFormValue {
        const date = new Date(this.form.value.date);
        const weight = this.form.value.weight;
        const consumed = this.form.value.consumed;

        const value: MeasurmentFormValue = { date, weight };

        if (typeof consumed === 'number' && consumed !== 0) {
            value.consumed = consumed;
        }

        return value;
    }
}
