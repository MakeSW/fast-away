import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin, Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Profile } from '../../../shared/model/profile.interface';
import { DatePipe } from '@angular/common';
import { MEASURING_REPOSITORY_TOKEN, MeasuringRepository } from '../../../shared/services/measuring-repository.interface';
import {
  PROFILE_REPOSITORY_TOKEN,
  ProfileRepository,
} from '../../../shared/services/profile-repository.interface';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'faw-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public profile: Profile;

  private spinner: HTMLIonLoadingElement;
  private subscription = new Subscription();

  constructor(
    @Inject(PROFILE_REPOSITORY_TOKEN) private readonly profileSrv: ProfileRepository,
    @Inject(MEASURING_REPOSITORY_TOKEN) private readonly measuringSrv: MeasuringRepository,
    private readonly loadingCtrl: LoadingController,
    private readonly translate: TranslateService,
    private readonly datePipe: DatePipe,
    private readonly alertCtrl: AlertController,
    private readonly toastCtrl: ToastController,
  ) {}

  ngOnInit() {
    this.setupPage();
  }

  ngOnDestroy(): void {
    if (this.subscription !== null) {
        this.subscription.unsubscribe();
        this.subscription = null;
    }
  }

  async promptMeasurementDeletion(): Promise<void> {
    const alert = await this.alertCtrl.create({
        message: this.translate.instant('profile_measurings_delete_all_prompt'),
        buttons: [
            {
                text: this.translate.instant('generic_cancel'),
                role: 'cancel',
            },
            {
                text: this.translate.instant('profile_measurings_delete_all'),
                handler: () => this.deleteAllMeasurmenets(),
            }
        ],
    });
    await alert.present();
  }

  private async deleteAllMeasurmenets(): Promise<void> {
    this.spinner = await this.loadingCtrl.create({
        message: this.translate.instant('profile_loading'),
    });

    this.measuringSrv.deleteAllMeasurements().subscribe(
        async success => {
            try {
                await this.spinner.dismiss();
                const key = success ? 'profile_measurings_deleted' : 'profile_measurings_deletion_failed';
                const toast = await this.toastCtrl.create({
                    message: this.translate.instant(key),
                });
                await toast.present();
            } catch (e) {
                // we do not care about a failed toast
            }
        }
    );
  }

  private async setupPage(): Promise<void> {
    this.spinner = await this.loadingCtrl.create({
      message: this.translate.instant('profile_loading'),
    });

    const $profile = this.profileSrv.getProfile();
    const $lang = this.translate.use(this.translate.currentLang);

    forkJoin([$profile, $lang]).subscribe(
      ([profile, _]: [Profile, any]) => {
        this.profile = profile;
        this.setupProfileForm();
        this.spinner.dismiss();
      },
      (error) => {
        console.error('Couldn\'t load profile', error);
        this.spinner.dismiss();
      }
    );
  }

  private setupProfileForm(): void {
    const keys: (keyof Profile)[] = [
      'weightUnit',
      'heightUnit',
      'height',
      'startingWeight',
      'birthday',
      'sex',
      'activityLevel',
    ];
    const group: { [key: string]: FormControl } = {};

    this.subscription.unsubscribe();
    this.subscription = new Subscription();

    keys.forEach((key: keyof Profile) => {
        const value = this.profile[key];

        if (key === 'birthday' && typeof value !== 'string') {
            const dateToken = this.datePipe.transform(value, 'yyyy-MM-dd');
            group[key] = new FormControl(dateToken);
        } else {
            group[key] = new FormControl(value);
        }

    });
    this.form = new FormGroup(group);

    this.subscription.add(this.form.valueChanges.pipe(delay(25)).subscribe((value: Profile) => {
        this.profile = value;
        this.profileSrv.updateProfile(this.profile);
    }));
  }
}
