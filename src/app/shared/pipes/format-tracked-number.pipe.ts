import { formatNumber, NumberFormatStyle } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'formatTrackedNumber'
})
export class FormatTrackedNumberPipe implements PipeTransform {
    constructor(
        private readonly translate: TranslateService,
    ) {}

  transform(value: NumberFormatStyle): unknown {
    if (typeof value !== 'number' || isNaN(value)) {
        return this.translate.instant('generic_not_tracked');
    }

    return formatNumber(value, 'en-US', '1.2-2');
  }
}
