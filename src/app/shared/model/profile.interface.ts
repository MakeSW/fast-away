import { WeightUnit } from './measuring.interface';

export type HeightUnit = 'cm' | 'inch';

export type Sex = 'Male' | 'Female';

export type ActivityLevel =
    'Sedentary' |
    'Lightly Active' |
    'Moderately Active' |
    'Very Active' |
    'Extremely Active';

export interface Profile {
    weightUnit: WeightUnit;
    heightUnit: HeightUnit;
    height: number;
    startingWeight: number;
    birthday: Date;
    sex: Sex;
    activityLevel: ActivityLevel;
}
