import { Component, Inject, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { forkJoin } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { Measuring } from '../../../shared/model/measuring.interface';
import { Profile } from '../../../shared/model/profile.interface';
import { WeightProjection } from '../../../shared/model/weight-projection.interface';
import { MEASURING_REPOSITORY_TOKEN, MeasuringRepository } from '../../../shared/services/measuring-repository.interface';
import { PROFILE_REPOSITORY_TOKEN, ProfileRepository } from '../../../shared/services/profile-repository.interface';
import { WeightProjectionService } from '../../../shared/services/weight-projection.service';
import { MeasurementFormComponent } from '../components/measurement-form/measurement-form.component';
import { ScrollHelperService } from '../../../shared/services/scroll-helper.service';
import { MeasurmeneFormValue } from '../../../shared/model/measurement-form-value.interface';
import { Status } from '../../../shared/model/status.interface';
import { StatusService } from '../../../shared/services/status.service';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'faw-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss'],
})
export class OverviewPageComponent implements OnInit {
    private static readonly numberOfProjections = 365;

    projections: WeightProjection[] = [];

    status: Status;

    public profile: Profile;

    private spinner: HTMLIonLoadingElement;


    constructor(
        @Inject(MEASURING_REPOSITORY_TOKEN) private readonly measuringSrv: MeasuringRepository,
        @Inject(PROFILE_REPOSITORY_TOKEN) private readonly profileSrv: ProfileRepository,
        private readonly loadingCtrl: LoadingController,
        private readonly translate: TranslateService,
        private readonly modalCtrl: ModalController,
        private readonly projectionSrv: WeightProjectionService,
        private readonly scrollHelperSrv: ScrollHelperService,
        private readonly statusSrv: StatusService,
    ) {}

    ngOnInit(): void {
        this.loadData();
    }

    public async showMeasuringDialog(measuring?: Measuring): Promise<void> {
        try {
            const date = measuring ? measuring.date : new Date();
            const consumed = measuring?.consumed || null;
            const weight = measuring?.weight || null;
            const modal = await this.modalCtrl.create({
                component: MeasurementFormComponent,
                componentProps: {
                    date,
                    consumed,
                    weight,
                },
            });

            await modal.present();
            const { data } = await modal.onWillDismiss();
            const isUpdate = !!data;

            if (isUpdate) {
                await this.updateMeasuring(data);
            }
        } catch(e: unknown) {
            console.error('Couldn\'t open measurement dialog', e);
        }
    }

    public async updateMeasuring(formVal: MeasurmeneFormValue): Promise<void> {
        await this.showLoading();

        const measuring: Measuring = {
            ...formVal,
            unit: this.profile.weightUnit,
        };
        this.measuringSrv.addOrUpdateMeasurement(measuring).subscribe(
            _ => {
                this.loadData();
            },
            error => {
                this.spinner.dismiss();
                console.error('Couldn\'t add measuring', error);
            },
        );
    }

    private async loadData(): Promise<void> {
        if (!this.spinner) {
            await this.showLoading();
        }

        const start = this.getStartDate();
        const dayBeforeStart = new Date(start);
        dayBeforeStart.setDate(dayBeforeStart.getDate() - 1);

        const $profile = this.profileSrv.getProfile();
        const $measurings = this.measuringSrv.getMeasurements(start);

        const $startMeasuring = this.profileSrv.getDefaultMeasurment(dayBeforeStart).pipe(
            mergeMap(measuring => this.measuringSrv.getLastMeasurement(dayBeforeStart, measuring)),
        );
        const $status = this.statusSrv.getStatus();

        forkJoin([ $profile, $measurings, $startMeasuring, $status ]).subscribe(
            ([ profile, measurings, startMeasuring, status  ]: [ Profile, Measuring[], Measuring, Status ]) => {
                this.profile = profile;
                this.status = status;
                this.projectionSrv.configure(
                    this.profile,
                    measurings,
                );
                this.projections = this.projectionSrv.getProjections(
                    startMeasuring,
                    measurings,
                    OverviewPageComponent.numberOfProjections,
                );

                this.spinner.dismiss();
                this.spinner = null;
                this.scrollHelperSrv.scrollToMeasurement(new Date());
            },
            error => {
                console.error('Couldn\'t load measurements', error);
                this.spinner.dismiss();
                this.spinner = null;
            },
        );
    }

    private getStartDate(): Date {
        const date = new Date();
        date.setDate(1);
        date.setMonth(date.getMonth() - 1);
        date.setHours(0, 0, 0, 0);
        return date;
    }

    private async showLoading(): Promise<void> {
        this.spinner = await this.loadingCtrl.create({
        message: this.translate.instant('overview_loading'),
        });
        await this.spinner.present();
    }
}
