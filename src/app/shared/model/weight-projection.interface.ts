import { Measuring } from './measuring.interface';

export interface WeightProjection extends Measuring {
    projectedWeight: number;
    projected?: boolean;
    caloricDeficit?: number;
    plannedIntake?: number;
    daysOfStickingToPlan?: number;
}
