import { DOCUMENT, DatePipe } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class ScrollHelperService {
  constructor(
      @Inject(DOCUMENT) private readonly document: Document,
      private readonly datePipe: DatePipe,
  ) {}

  scrollToMeasurement(date: Date): void {
      of(null).pipe(
          delay(25),
      ).subscribe(_ => {
        const dateToken = this.datePipe.transform(date, 'yyyy-MM-dd');
        const selector = `[measurement-date="${dateToken}"]`;
        const element = this.document.querySelector(selector);

        if (!!element) {
            element.scrollIntoView({ behavior: 'smooth' });
        }
      });
  }
}
