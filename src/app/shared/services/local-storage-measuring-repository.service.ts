import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Measuring } from '../model/measuring.interface';
import { MeasuringRepository } from './measuring-repository.interface';

@Injectable()
export class LocalStorageMeasuringRepositoryService implements MeasuringRepository {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  private static readonly MEASURING_KEY = 'measuring';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  private static readonly MESURING_VERSION = '0.0.1';

  constructor() {}
    deleteMeasurement(_: Measuring): Observable<boolean> {
        throw new Error('Method not implemented.');
    }

    deleteAllMeasurements(): Observable<boolean> {
        let error = null;

        try {
            this.updateAllMeasurements([]);
        } catch(e) {
            console.error('Couldn\'t delete all measurements', e);
            error = e;
        }

        return of(!error);
    }

    getLatestMeasurement(): Observable<Measuring> {
        const allMeasurements = this.getAllMeasurements() || [];
        const latestMeasurement = allMeasurements[allMeasurements.length - 1] || null;

        return of(latestMeasurement);
    }

    addOrUpdateMeasurement(measuring: Measuring): Observable<Measuring> {
        return this.updateMeasurement(measuring);
    }

    getLastMeasurement(until: Date, fallback: Measuring): Observable<Measuring> {
        const allMeasurements = this.getAllMeasurements();
        let last: Measuring = null;

        for ( const measuring of allMeasurements ) {
            if (measuring.date <= until) {
                last = measuring;
            } else {
                break;
            }
        }

        return of(last || fallback);
    }

    getMeasurements(startDate: Date, endDate?: Date): Observable<Measuring[]> {
        const allMeasurements = this.getAllMeasurements();

        console.log('measurements', allMeasurements);

        const filteredMeasurements = allMeasurements
            .filter(m => m.date >= startDate)
            .filter(m => !endDate || m.date <= endDate);

        return of(filteredMeasurements);
    }

    addMeasurement(measuring: Measuring): Observable<Measuring> {
        const allMeasurements = this.getAllMeasurements();
        const nextIdx = allMeasurements.length;

        measuring.idx = nextIdx;
        allMeasurements.push(measuring);
        this.updateAllMeasurements(allMeasurements);
        return of(measuring);
    }

    updateMeasurement(measuring: Measuring): Observable<Measuring> {
        const allMeasurements = this.getAllMeasurements();
        const measurementIdx: number = allMeasurements.findIndex(m =>
            m.date.getFullYear() === measuring.date.getFullYear() &&
            m.date.getMonth() === measuring.date.getMonth() &&
            m.date.getDate() === measuring.date.getDate()
        );

        if (measurementIdx >= 0) {
            allMeasurements[measurementIdx] = measuring;
            this.updateAllMeasurements(allMeasurements);

            return of(measuring);
        }

        return this.addMeasurement(measuring);
    }

    private getAllMeasurements(): Measuring[] {
        const dataToken = localStorage.getItem(this.getKey());

        if (!!dataToken) {
            try {
                const measurements: Measuring[] = JSON.parse(dataToken);

                return (measurements || []).map(m => {
                    m.date = (typeof m.date === 'string') ? new Date(m.date) : m.date;
                    return m;
                });
            } catch(e) {
                console.error('Couldn\'t retreive measurements', e);
            }
        }

        return [];
    }

    private updateAllMeasurements(measurements: Measuring[]): void {
        localStorage.setItem(this.getKey(), JSON.stringify(measurements));
    }

    private getKey(): string {
        return `${LocalStorageMeasuringRepositoryService.MEASURING_KEY}_${LocalStorageMeasuringRepositoryService.MESURING_VERSION}`;
    }
}
