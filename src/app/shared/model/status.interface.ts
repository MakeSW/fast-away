export type Status = EmptyStatus | StatusData;

type EmptyStatus = 'Empty';

export interface StatusData {
    weight: number;
    bmi: number;
    lostWeight: number;
}
