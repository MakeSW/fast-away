import { Inject, Injectable } from '@angular/core';
import { PROFILE_REPOSITORY_TOKEN, ProfileRepository } from './profile-repository.interface';
import { MEASURING_REPOSITORY_TOKEN, MeasuringRepository } from './measuring-repository.interface';
import { Observable, forkJoin } from 'rxjs';
import { Status } from '../model/status.interface';
import { Measuring } from '../model/measuring.interface';
import { Profile } from '../model/profile.interface';
import { map } from 'rxjs/operators';
import { UnitConversionService } from './unit-conversion.service';

@Injectable()
export class StatusService {
  constructor(
      @Inject(PROFILE_REPOSITORY_TOKEN) private readonly profileSrv: ProfileRepository,
      @Inject(MEASURING_REPOSITORY_TOKEN) private readonly measuringSrv: MeasuringRepository,
      private readonly unitSrv: UnitConversionService,
  ) { }

  getStatus(): Observable<Status> {
    return forkJoin([
        this.profileSrv.getProfile(),
        this.measuringSrv.getLatestMeasurement(),
    ]).pipe(
        map(([ profile, measuring ]) => {
            if (!profile) {
                return 'Empty';
            }

            const weight = measuring?.weight || profile.startingWeight;
            const lostWeight = Math.max(0, profile.startingWeight - weight);

            return {
                bmi: this.calculateBmi(weight, profile),
                weight,
                lostWeight,
            };
        }),
    );
  }

  private calculateBmi(currentWeight: number, profile: Profile): number {
      const weight = profile.weightUnit === 'kg' ? currentWeight : this.unitSrv.convertPoundToKg(currentWeight);
      const height = (profile.heightUnit === 'cm' ? profile.height : this.unitSrv.convertInchesToCm(profile.height)) / 100.0;

    console.log(weight, height, weight / (height * height));

      return weight / (height * height);
  }
}
