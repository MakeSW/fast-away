import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { Measuring } from '../model/measuring.interface';
import { Profile } from '../model/profile.interface';

export const PROFILE_REPOSITORY_TOKEN = new InjectionToken<ProfileRepository>('ProfileRepository');

export interface ProfileRepository {
    getProfile(): Observable<Profile>;
    updateProfile(profile: Profile): Observable<Profile>;
    getDefaultMeasurment(date: Date): Observable<Measuring>;
}

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function createProfile(): Profile {
    const defaultBirthday = new Date();
    defaultBirthday.setFullYear(defaultBirthday.getFullYear() - 20);
    defaultBirthday.setMonth(5);
    defaultBirthday.setDate(1);

    return {
        weightUnit: 'kg',
        heightUnit: 'cm',
        activityLevel: 'Sedentary',
        birthday: defaultBirthday,
        sex: 'Male',
        startingWeight: 80,
        height: 175,
    };
}
