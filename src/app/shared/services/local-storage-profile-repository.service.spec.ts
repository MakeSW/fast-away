import { TestBed } from '@angular/core/testing';

import { LocalStorageProfileRepositoryService } from './local-storage-profile-repository.service';

describe('LocalStorageProfileRepositoryService', () => {
  let service: LocalStorageProfileRepositoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageProfileRepositoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
