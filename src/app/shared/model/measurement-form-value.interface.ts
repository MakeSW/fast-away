export interface MeasurmeneFormValue {
    date: Date;
    weight: number;
    consumed?: number;
}
