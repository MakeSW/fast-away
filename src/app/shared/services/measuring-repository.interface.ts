import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { Measuring } from '../model/measuring.interface';

export const MEASURING_REPOSITORY_TOKEN = new InjectionToken<MeasuringRepository>('MeasuringRepository');

export interface MeasuringRepository {
    getMeasurements(startDate: Date, endDate?: Date): Observable<Measuring[]>;

    addMeasurement(measuring: Measuring): Observable<Measuring>;

    updateMeasurement(measuring: Measuring): Observable<Measuring>;

    getLatestMeasurement(): Observable<Measuring>;

    getLastMeasurement(until: Date, fallback: Measuring): Observable<Measuring>;

    addOrUpdateMeasurement(measuring: Measuring): Observable<Measuring>;

    deleteMeasurement(measuring: Measuring): Observable<boolean>;

    deleteAllMeasurements(): Observable<boolean>;
}
